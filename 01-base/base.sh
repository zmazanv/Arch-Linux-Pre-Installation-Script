#!/bin/bash

###############################################################################
# BASE ########################################################################
###############################################################################
echo 'Please enter the device name of the drive to install to (including /dev/):'
read device_name
echo "Your chosen hostname is ${device_name}..."

pacman -Sy || exit

wipefs -a "${device_name}"
fdisk "${device_name}"

mkfs.fat -F 32 -n FAT32_EFI "${device_name}p1"
mkfs.ext4 -L ext4_boot "${device_name}p2"
cryptsetup luksFormat "${device_name}p3"
cryptsetup open "${device_name}p3" cryptlvm

pvcreate -v /dev/mapper/cryptlvm
vgcreate -v volgroup /dev/mapper/cryptlvm
lvcreate -v -L 32G volgroup -n swap
lvcreate -v -L 150G volgroup -n root
lvcreate -v -L 700G volgroup -n home
lvcreate -v -L 50G volgroup -n docker
lvcreate -v -L 20G volgroup -n mariadb

mkswap -L lv_swap /dev/volgroup/swap
mkfs.btrfs -L lv_btrfs_root /dev/volgroup/root
mkfs.btrfs -L lv_btrfs_home /dev/volgroup/home
mkfs.btrfs -L lv_btrfs_docker /dev/volgroup/docker
mkfs.ext4 -L lv_ext4_mariadb /dev/volgroup/mariadb

reflector --save /etc/pacman.d/mirrorlist --country 'United States' --latest 10 --sort rate

mount -v /dev/volgroup/root /mnt
mount -v --mkdir /dev/volgroup/home /mnt/home
mount -v --mkdir /dev/nvme0n1p2 /mnt/boot
mount -v --mkdir /dev/nvme0n1p1 /mnt/boot/efi
mount -v --mkdir /dev/volgroup/docker /mnt/var/lib/docker
mount -v --mkdir /dev/volgroup/mariadb /mnt/var/lib/mysql
swapon /dev/volgroup/swap

pacstrap -Ki /mnt base base-devel linux linux-lts linux-headers linux-lts-headers lvm2 git neovim vim btrfs-progs

genfstab -U /mnt >>/mnt/etc/fstab
###############################################################################
